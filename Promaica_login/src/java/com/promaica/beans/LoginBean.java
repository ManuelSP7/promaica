/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promaica.beans;

import com.promaica.dao.LoginDAO;
import com.promaica.dao.UsersDAO;
import com.promaica.persistencias.UsersTbl;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.application.FacesMessage;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.PrimeFaces;

/**
 *
 * @author manuel.suarezusam
 */
@ManagedBean
@SessionScoped
public class LoginBean {
    
    private String username;
    private String password;
    private static int id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        LoginBean.id = id;
    }

    public LoginBean() {
    }

   public String login() {
        UsersDAO UsuarioDAO = new UsersDAO();
        UsersTbl usuario = UsuarioDAO.iniciarSesion(username, password);
        FacesMessage message = null;
        boolean loggedIn;
        System.out.println("datos usuario: " + usuario.toString());

        try {

            if (usuario != null) {
                id = usuario.getId();
                
                loggedIn = true;

                FacesContext.getCurrentInstance().getExternalContext().redirect("Home.xhtml");
                message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Bienvenido", usuario.getUsername());
                return "Home.xhtml";
            } else {
                loggedIn = false;
                message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Usuario o contraseña incorrecta");
            }

            FacesContext.getCurrentInstance().addMessage(null, message);
            PrimeFaces.current().ajax().addCallbackParam("loggedin", loggedIn);
        } catch (Exception e) {

        }
        return null;
    }
    
    
}
