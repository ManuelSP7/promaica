/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promaica.beans;

import com.promaica.dao.UsersDAO;
import com.promaica.persistencias.UsersTbl;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author manuel.suarezusam
 */

@ManagedBean
@ViewScoped
public class UserBean {
    private List<UsersTbl> listaUser;
    private UsersDAO userDao;
    private UsersTbl user;
    private String username;
    private String password;
    private String accion;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    @PostConstruct
    public void init(){
        userDao = new UsersDAO();
        //listaUser = userDao.buscarTodo();
        this.user = new UsersTbl();
        this.user.setId(0);
        accion = "Registrar";
    }
}
