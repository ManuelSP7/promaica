/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promaica.beans;

import com.promaica.dao.ProductoDAO;
import com.promaica.persistencias.ProductTbl;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author manuel.suarezusam
 */
@ManagedBean
@ViewScoped

public class ProductoBean {
    
    private List<ProductTbl> listaProducto;
    private ProductoDAO productoDAO;
    private ProductTbl producto;
    private String accion;
    private String message;
    
    @PostConstruct
    public void init(){
        productoDAO = new ProductoDAO();
        listaProducto = productoDAO.buscarTodo();
        this.producto = new ProductTbl();
        this.producto.setId(0);
        accion = "Registrar";
    }
    
    public void limpiarFormulario(){
        this.listaProducto = productoDAO.buscarTodo();
        this.producto = new ProductTbl();
        this.producto.setId(0);
        accion = "Registrar";
    }
    
    public void accionFormulario(){
        if(accion.equals("Registrar")){
            productoDAO.insertar(this.producto);
        }
        limpiarFormulario();
        
        
    }
    
    private void insertar(){
        productoDAO.insertar(this.producto);
        this.listaProducto = productoDAO.buscarTodo();
        String adv = "";
        if(productoDAO.insertar(producto)==true){
            adv="¡Producto Creado Exitosamente!";
        }else{
            adv="No se pudo crear el producto.";
        }
        FacesMessage msg = new FacesMessage(adv);  
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public ProductoBean() {
        super();
    }

    public List<ProductTbl> getListaProducto() {
        return listaProducto;
    }

    public void setListaProducto(List<ProductTbl> listaProducto) {
        this.listaProducto = listaProducto;
    }

    public ProductoDAO getProductoDAO() {
        return productoDAO;
    }

    public void setProductoDAO(ProductoDAO productoDAO) {
        this.productoDAO = productoDAO;
    }

    public ProductTbl getProducto() {
        return producto;
    }

    public void setProducto(ProductTbl producto) {
        this.producto = producto;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
    public void saveMessage() {
        FacesContext context = FacesContext.getCurrentInstance();
         message="¡Vista en Construccion, Proximamente!";
        context.addMessage(null, new FacesMessage(message) );
    }
}
