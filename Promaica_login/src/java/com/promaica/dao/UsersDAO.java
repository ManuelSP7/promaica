/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promaica.dao;

import com.promaica.persistencias.UsersTbl;
import com.promaica.util.JPAUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author manuel.suarezusam
 */
public class UsersDAO {

    public UsersTbl consultarid(Integer id) {
        EntityManager em = JPAUtil.getJPAFactory().createEntityManager();
        UsersTbl usuario = null;
        em.getTransaction().begin();
        try {
            usuario = em.find(UsersTbl.class, id);
            em.getTransaction().commit();
            System.out.println("Su consulta por ID Fue exitosa");
        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println("Error al consultar por ID " + e);
        } finally {
            em.close();
        }
        return usuario;
        
    }
    
    public UsersTbl iniciarSesion(String username, String password){
        EntityManager em = JPAUtil.getJPAFactory().createEntityManager();
        UsersTbl usuario = new UsersTbl();
        int id = 0;
        try {
            em.getTransaction().begin();
            id = Integer.parseInt(em.createNativeQuery("select id from users_tbl where username = '"+username+"' and password = '"+password+"';").getSingleResult().toString());
            em.getTransaction().commit();
            usuario = this.consultarid(id);
        } catch (NumberFormatException e) {
            em.getTransaction().rollback();
            usuario = null;
            System.out.println("Error: "+e);
        }finally{
            em.close();
        }
        return usuario;
    }
}
