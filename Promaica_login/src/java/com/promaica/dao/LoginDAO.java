/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promaica.dao;

import com.promaica.persistencias.UsersTbl;
import com.promaica.util.JPAUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;


/**
 *
 * @author manuel.suarezusam
 */

public class LoginDAO {
    
    public UsersTbl consultarid(Integer id) {
        EntityManager em = JPAUtil.getJPAFactory().createEntityManager();
        UsersTbl usuario = null;
        em.getTransaction().begin();
        try {
            usuario = em.find(UsersTbl.class, id);
            em.getTransaction().commit();
            System.out.println("Su consulta por ID Fue exitosa");
        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println("Error al consultar por ID " + e);
        } finally {
            em.close();
        }
        return usuario;
        
    }
    
    
}
