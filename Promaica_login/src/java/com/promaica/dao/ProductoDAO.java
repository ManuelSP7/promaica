/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promaica.dao;

import com.promaica.persistencias.ProductTbl;
import com.promaica.util.JPAUtil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

/**
 *
 * @author manuel.suarezusam
 */
public class ProductoDAO {

    public List<ProductTbl> buscarTodo() {
        EntityManagerFactory emf = JPAUtil.getJPAFactory();
        EntityManager em = emf.createEntityManager();
        TypedQuery consulta = em.createQuery("Select e from ProductTbl e", ProductTbl.class);
        List<ProductTbl> listaProducto = consulta.getResultList();
        return listaProducto;
    }

    public boolean insertar(ProductTbl producto) {
        EntityManagerFactory emf = JPAUtil.getJPAFactory();
        EntityManager em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            em.persist(em.merge(producto));
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println("Error: "+e);
            return false;
        }finally{
            em.close();
        }
    }
}
