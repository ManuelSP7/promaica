/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.promaica.persistencias;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author manuel.suarezusam
 */
@Entity
@Table(name = "product_tbl")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProductTbl.findAll", query = "SELECT p FROM ProductTbl p")
    , @NamedQuery(name = "ProductTbl.findById", query = "SELECT p FROM ProductTbl p WHERE p.id = :id")
    , @NamedQuery(name = "ProductTbl.findByName", query = "SELECT p FROM ProductTbl p WHERE p.name = :name")
    , @NamedQuery(name = "ProductTbl.findByPrice", query = "SELECT p FROM ProductTbl p WHERE p.price = :price")
    , @NamedQuery(name = "ProductTbl.findByDescription", query = "SELECT p FROM ProductTbl p WHERE p.description = :description")})
public class ProductTbl implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private String price;
    @Column(name = "description")
    private String description;

    public ProductTbl() {
    }

    public ProductTbl(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductTbl)) {
            return false;
        }
        ProductTbl other = (ProductTbl) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Persistencias.ProductTbl[ id=" + id + " ]";
    }
    
}
